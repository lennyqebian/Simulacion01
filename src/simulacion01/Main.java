package simulacion01;

import java.util.Scanner;
import javax.script.ScriptException;

/**
 *
 * @author leonardoquevedo
 */
public class Main {

    /**
     * @param args the command line arguments
     * @throws javax.script.ScriptException
     */
    public static void main(String[] args) throws ScriptException {
        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter("\n\r|\n");
        System.out.print("¿Cuántos números aleatorios?: ");
        int n = scanner.nextInt();
        System.out.print("Semilla x_0: ");
        double x_0 = scanner.nextDouble();
        System.out.println("IMPORTANTE: Para los siguientes campos, asegúrate de escribir en lenguaje JavaScript...");
        System.out.print("Función de transición T(x): ");
        String TxStr = scanner.next();
        System.out.print("Función de generación G(x): ");
        String GxStr = scanner.next();
        RandomGenerator generator = new RandomGenerator(x_0, TxStr, GxStr);

        try {
            String U = generator.randomStr(n);
            System.out.println(U);
        } catch (ScriptException|ClassCastException ex) {
            System.err.println("¡Hay un error de JavaScript o de conversión de tipos de datos en la función que ingresaste! :/");
            throw ex;
        }
    }
}
