package simulacion01;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 *
 * @author leonardoquevedo
 */
public class RandomGenerator {

    protected double x_0;

    protected String TxStr;

    protected String GxStr;

    protected final ScriptEngineManager manager;

    protected final ScriptEngine engine;

    public RandomGenerator(double x_0, String TxStr, String GxStr) {
        this.x_0 = x_0;
        this.TxStr = TxStr;
        this.GxStr = GxStr;
        this.manager = new ScriptEngineManager();
        this.engine = this.manager.getEngineByName("JavaScript");
    }

    public double[] random(int n) throws ScriptException, ClassCastException {
        ArrayList<Double> X = new ArrayList();
        double[] U = new double[n];
        double X_n, U_n;

        X.add(this.x_0);

        for (int i = 0; i < n; i++) {
            X_n = this.runJsFunc(this.TxStr, X.get(i));
            U_n = this.runJsFunc(this.GxStr, X_n);
            X.add(X_n);
            U[i] = U_n;
        }
        return U;
    }
    
    public String randomStr(int n) throws ScriptException, ClassCastException {
        double[] U = this.random(n);
        int decimals = 4;
        DecimalFormat formatter = new DecimalFormat("#." + String.format("%0" + decimals + "d", 0).replace("0", "#"));
        
        if (U.length > 1) {
            List<String> output = Arrays.stream(U).boxed().map((aDouble) -> {
                return formatter.format(aDouble);
            })
                    .collect(Collectors.toList());
            return "Números aleatorios generados: \n\n" 
                    + String.join("\n", output);
        }
        return "Número aleatorio generado: " + Double.toString(U[0]);
    }

    public double random() throws ScriptException {
        return this.random(1)[0];
    }

    protected double runJsFunc(String TxStr, double X_i) throws ScriptException, ClassCastException {
        String script = "var x = " + Double.toString(X_i) + ";\n";
        script += TxStr + ";";
        return (double) this.engine.eval(script);
    }
}
